# Get Dumpster weight


```plantuml
@startuml

participant Service
participant Edge

Service->Edge : Request dumpster weight
Service<--Edge : Send dumpster weight 

opt Weight over limit
    Service->Service : Set dumpster unavailable
end

@enduml
```


# Deposit

```plantuml
@startuml

actor User
participant MobileApp
participant Service
participant Edge
participant Controller

User->MobileApp : Request dumpster use
MobileApp->Service : Request token

ref over Edge, Service : Get dumpster weight

alt Dumpster available

MobileApp<--Service : Send token
User->MobileApp : Select trash type
MobileApp->>Controller : Send dumpster activate request
Controller->Controller : Turn on trash type LED
Controller->Controller : Open dumpster
Controller->Controller : Wait for Tdeliver seconds
MobileApp->>Controller : Extend time
Controller -> Controller : Extend time

Controller->Controller : Close dumpster
MobileApp<--Controller : Report successful dumpster activation
User<--MobileApp : Show successful dumpster activation
MobileApp->>Service : Report successful dumpster activation

ref over Edge, Service : Get dumpster weight
Service->Service : Save dumpster weight

opt Weight changed
Service->Service: Increase trash deposit count
end

else Dumpster unavailable

MobileApp<--Service : Send error
User<--MobileApp : Show error

@enduml
```

# Administration

## Request dumpster info

```plantuml
@startuml

actor Admin
participant Dashboard
participant Service

Admin->Dashboard : Request dumpster info of last Ndays
Dashboard->Service : Request dumpster info of last Ndays
Dashboard<--Service : Send dumpster info of last Ndays
Admin<--Dashboard : Show dumpster info of last Ndays

@enduml
```

## Reset dumpster to available

```plantuml
@startuml

actor Admin
participant Dashboard
participant Service
participant Edge

Admin->Dashboard : Reset dumpster to available
Dashboard->Service : Reset dumpster to available
Service->Edge : Reset dumpster to available
Service<--Edge : Send dumpster status
Dashboard<--Service : Send dumpster status
Admin<--Dashboard : Show dumpster status

@enduml
```

## Set dumpster to not available

```plantuml
@startuml

actor Admin
participant Dashboard
participant Service
participant Edge

Admin->Dashboard : Set dumpster to not available
Dashboard->Service : Set dumpster to not available
Service->Edge : Set dumpster to not available
Service<--Edge : Send dumpster status
Dashboard<--Service : Send dumpster status
Admin<--Dashboard : Show dumpster status

@enduml
```
