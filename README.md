# Smart Dumpster

## LaTeX report

**NOTE**: you can also use `kile` to write LaTeX, I think it's better (mostly
for Vi mode)

- install
  - `texlive-core`
  - `texlive-latexextra`
  - `minted`
  - `plantuml`
  - `texstudio`
  - `graphviz`
- download italian dictionary [from libreoffice](https://extensions.libreoffice.org/en/extensions/show/italian-dictionary-thesaurus-hyphenation-patterns) and put it in the right folder
  ([credits](https://tex.stackexchange.com/questions/87650/dictionary-for-texstudio-no-dictionary-available/87652#87652)
- import the texstudio profile (Options > Load profile >
  [`texstudio.txsprofile`](./texstudio.txsprofile)) in TeXstudio. It contains
  - run shell instructions to compile LaTeX report with plantuml diagrams
  - solarized dark theme from [Francis-Hsu/TeXstudio_Solarized](https://github.com/Francis-Hsu/TeXstudio_Solarized)
- restart texstudio
- run with F5

## Analysis and project diagrams

- [Class diagram](./CLASS_DIAGRAM.md)
- [Sequence diagram](./SEQUENCE_DIAGRAM.md)
- [Status diagram](./STATUS_DIAGRAM.md)

## Fritzing

### Controller

![](./fritzing/controller.png)

### Edge

![](./fritzing/edge.png)
