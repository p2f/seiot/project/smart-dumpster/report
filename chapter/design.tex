\clearpage

\section{Design} \label{sec:Design}

\subsection{Architettura}

\subsubsection{Controller}

Per ciò che concerne il Controller, in fase di analisi la macchina a stati finiti è stata integrata con una modellazione a loop con decomposizione per task. Ciò ha permesso di semplificare la progettazione dettagliata del modulo nonché la sua implementazione.

\begin{figure}[!htb]
	\centering
	\begin{plantuml}
		@startuml
		scale 0.5
skinparam groupInheritance 3
top to bottom direction

interface Task {
	+ void init()
	+ void tick()
}

interface Scheduler {
	- Task *tasks
	+ void init()
	+ void schedule()
	+ bool addTask()
}

Scheduler "1" *-- "*" Task

class OpeningTask
class OpenedTask
class ClosingTask
class ClosedTask

Task <|-- OpeningTask
Task <|-- OpenedTask
Task <|-- ClosingTask
Task <|-- ClosedTask
		@enduml
	\end{plantuml}
	\caption{Decomposizione a task del modulo Controller.}
	\label{fig:controller-class-high-1}
\end{figure}

\subsubsection{Edge}

Per il modulo \textit{Edge} è necessario distinguere la logica di funzionamento su due livelli, quello più vicino al circuito e al controllo dei componenti fisici e il livello di controllo dello stato del Dumpster.

Nonostante tali aspetti siano correlati (poiché, ad esempio, nello stato "available" è necessario accendere il corrispondente LED), è stato scelto di implementare la \textit{business logic} della gestione dello stato del Dumpster all'interno del modulo Service, al fine di centralizzare tale aspetto.

La gestione dei componenti fisici, invece, è stata implementata direttamente sul dispositivo mediante un modello a \textit{superloop} e fornisce un'interfacciamento Web con REST API.

\subsubsection{Service}

Il modulo \textit{Service} integra la gestione e presentazione della \textbf{Dashboard}, ovvero l'interfaccia utente dedicata all'amministratore di sistema.

Tale modulo segue il \textit{pattern} architetturale MVC, fornendo un'interfacciamento basato su tecnologie Web (HTTP) per la gestione dei Dumpster e dei Depositi.

\subsection{Design dettagliato}

Nei diagrammi che seguono vengono illustrati tutti i task del sistema e i componenti hardware/software, opportunamente modellati ad oggetti, con cui ognuno di essi interagisce.

\subsubsection{Controller}\label{sec:controllerdesign}

Come è visibile dalla Figura \ref{fig:controller-class-high-2} il Controller è diviso in quattro task:

\begin{itemize}
	\item Apertura del Dumpster: \textit{OpeningTask}
	\item Chiusura del Dumpster: \textit{ClosingTask}
	\item Attesa di richieste: \textit{ClosedTask}
	\item Deposito dei rifiuti: \textit{OpenedTask}
\end{itemize}

Ognuno di questi \textit{task} utilizza la classe \textit{JsonMsgService} (basata su \textit{BluetoothMsgService}) per la comunicazione con l'Applicazione tramite messaggi \textit{JSON}.

Utilizzando la funzione \texttt{isJsonAvailable()}, \textit{JsonMsgService} interroga il bluetooth; ad ogni nuovo messaggio ricevuto, la medesima funzione scompone la stringa ricevuta negli attributi 

\begin{itemize}
	\item \texttt{request}
	\item \texttt{amount}
	\item \texttt{event}
	\item \texttt{trashType}
\end{itemize}

Mediante gli opportuni \textit{getter}, i vari task utilizzeranno le informazioni appena salvate.

Alcuni task hanno la necessità di inviare messaggi all'Applicazione; questa funzione è ottenuta grazie ai metodi \texttt{sendEvent(String)} e \texttt{sendExtend(uint)}. \texttt{sendEvent(String)} viene utilizzato per la comunicazione dei cambiamenti di stato (opened, closed, opening, closing). \texttt{sendExtend(uint)} è invece usata nel caso in cui la richiesta di estensione di tempo superi i limiti definiti. In questo caso il Controller userà \texttt{sendExtend(uint)} per inviare il quantitativo di tempo rimanente (il quale sarà diverso dall'ammontare richiesto).

La conferma di ricezione dei messaggi avviene mediante un sistema di \textit{echo}: tutti i messaggi (eccetto quelli di chiusura della comunicazione) ricevuti da uno degli endpoint  devono essere rispediti al mittente, come prova della loro ricezione. Questo comportamento è ottenuto grazie al metodo \texttt{sendEcho()} che invia l'ultimo messaggio ricevuto al mittente.

Per ulteriori dettagli sul processo di comunicazione tra Controller e App consultare il paragrafo \ref{par:app-controller-communication}.

\begin{figure}[!htb]
	\centering
	\begin{plantuml}
		@startuml
class OpeningTask
class ClosingTask

enum TrashType {
	PAPER
	PLASTIC
	GLASS
}

enum StateType {
	CLOSED
	OPENING
	OPENED
	CLOSING
}

interface ServoMotor {
	+ uint getPosition()
	+ void setPosition(uint angle)
	+ void moveLeft()
	+ void moveRight()
}

class ServoMotorImpl {
	- uint pin
	- ServoTimer2 motor
	+ ServoImpl(uint pin)
	+ uint getPosition()
	+ void setPosition(uint angle)
	+ void moveLeft()
	+ void moveRight()
}

class ServoTimer2 <<external>> {
	+ attach();
	+ detach();
	+ write(int);
}

interface Led {
	+ void switchOn()
	+ void switchOff()
	+ bool isTurnedOn()
}

class LedImpl {
	- uint pin
	- bool isOn
	+ LedImpl(uint pin)
	+ void switchOn()
	+ void switchOff()
	+ bool isTurnedOn()
}

interface JsonMsgService {
	+ String getEvent()
	+ TrashType getTrashType()
	+ String getRequest()
	+ uint getAmount()
	+ bool isJsonAvailable()
	+ void sendEvent(String)
	+ void sendExtend(uint)
	+ void sendEcho()
}

interface BluetoothMsgService {
	+ void init()
	+ bool isMsgAvailable()
	+ Msg* receiveMsg()
	+ void sendMsg(String)
}

TrashType <.. OpeningTask: <<use>>
TrashType <.. ClosingTask: <<use>>
OpeningTask "1" o-- "1" ServoMotor
ClosingTask "1" o-- "1" ServoMotor
OpeningTask "3" o-- "1" Led
ClosingTask "3" o-- "1" Led
ServoMotor <|-- ServoMotorImpl
Led <|-down- LedImpl
ServoMotorImpl "1" o-down- "1" ServoTimer2 : <<external>>

OpeningTask ..> JsonMsgService : <<use>>
OpenedTask ..> JsonMsgService : <<use>>
ClosingTask ..> JsonMsgService : <<use>>
ClosedTask ..> JsonMsgService : <<use>>

OpeningTask .up.> StateType : <<use>>
OpenedTask .up.> StateType : <<use>>
ClosingTask .up.> StateType : <<use>>
ClosedTask .up.> StateType : <<use>>

class BluetoothMsgService

JsonMsgService *-- BluetoothMsgService
		@enduml
	\end{plantuml}
	\caption{Diagramma delle classi dettagliato del modulo Controller.}
	\label{fig:controller-class-high-2}
\end{figure}

\clearpage

\subsubsection{Edge} \label{sec:edge-detailed}

Il modulo Edge segue una modellazione orientata agli oggetti per la gestione dei componenti fisici e implementa l'architettura Web of Things per l'interfacciamento via Web di tali parti.

\begin{figure}[!htb]
	\centering
	\begin{plantuml}
		@startuml
		scale 2
class Edge {}

interface AsyncWiFiManager << external >> {
	+ bool autoConnect(char*, char*)
}

interface WebThingAdapter << external >> {
	+ void begin()
}
interface ThingDevice {
	+ String description
}
interface ThingProperty {
	+ String title
	+ void setValue(ThingPropertyValue)
	+ ThingPropertyValue changedValueOrNull()
	+ ThingPropertyValue getValue()
}
interface ThingPropertyValue {}

interface Led {
	+ void switchOn()
	+ void switchOff()
	+ bool isTurnedOn()
}

class LedImpl {
	- uint pin
	- bool isOn
	+ LedImpl(uint pin)
	+ void switchOn()
	+ void switchOff()
	+ bool isTurnedOn()
}

Led <|-- LedImpl
Edge "1" *-right- "1" Potentiometer
Edge "1" *-- "2" Led
Edge "1" *-left- "1" AsyncWiFiManager
Edge "1" *-- "1" WebThingAdapter
WebThingAdapter "1" o-- "3" ThingDevice
ThingDevice o-- ThingProperty
ThingProperty o-- ThingPropertyValue

Potentiometer <|-- PotentiometerImpl

interface Potentiometer {
	+ uint read()
}

class PotentiometerImpl {
	- uint pin
	- uint min
	- uint max
	+ uint PotentiometerImpl(uint pin, uint min, uint max)
	+ uint read()
}
		@enduml
	\end{plantuml}
	\caption{Diagramma delle classi del modulo Edge.}
	\label{fig:edge-class-detailed}
\end{figure}

\paragraph{Web of Things}

Oltre a fornire delle REST API per i componenti del sistema (di fatto applicando all'Internet of Things il concetto di "web" come una serie di risorse identificate da un URL ed interconnesse), Web of Things mira a fornire un modello dei dati ed un interfacciamento comune a tutti i dispositivi, garantendo interoperabilità.

Ed è così che ad ogni "oggetto" è possibile associare un insieme di informazioni semanticamente rilevanti, tra cui:

\begin{itemize}
	\item \texttt{Property}: Attributo di un "oggetto", ha tipo, unità di misura e il range di valori che può assumere (ad esempio la proprietà "on" di tipo booleano che esprime se un LED è acceso o spento)
	\item \texttt{Action}: Una funzione che può essere eseguita sul device eventualmente con dati in input (ad esempio il fading di un LED di cui si specifica il livello e la durata)
	\item \texttt{Event}: Un fenomeno registrato dal dispositivo (ad esempio il surriscaldamento di un componente)
\end{itemize}

Per ulteriori informazioni si consultino il documento di specifica a cura del consorzio W3C (https://www.w3.org/TR/wot-thing-description/) e l'implementazione proposta da Mozilla (https://iot.mozilla.org/wot/) utilizzata in questa sede.

\subparagraph{API}

Di seguito vengono dunque mostrate le risorse a cui il modulo Service accede per ottemperare alle specifiche di progetto.

Per ottenere il peso del Dumpster viene effettuata una chiamata HTTP GET alla risorsa \texttt{/things/potentiometer/properties/weight}.

Per accendere / spegnere un LED è necessario accedere alla risorsa \texttt{/things/\\lednotavailable/properties/on} con una chiamata HTTP PUT che ha come corpo della richiesta il seguente messaggio in formato JSON:

\begin{minted}{json}
{
    "on": true
}
\end{minted}

\subsubsection{Service}

\paragraph{API}

Il modulo "Service" interagisce con l'applicazione usata dall'utente e il componente "Edge" al fine di completare l'operazione di deposito.

Inoltre, gli utenti amministratori devono poter interagire con elevati privilegi operativi sul sistema attraverso la \textbf{Dashboard}, che andrà quindi ad interrogare il modulo Service.

A tal proposito è stato quindi ideato un opportuno interfacciamento basato su chiamate HTTP.

\subparagraph{Interazione tra App e Service}

Per effettuare un deposito, l'applicazione dovrà richiedere un token che autorizzerà l'utente ad interagire con il Dumpster. Tale operazione viene effettuata con una chiamata HTTP GET alla risorsa \texttt{/dumpsters/getToken}. Se il dumpster non è riservato ad un altro utente per un'operazione di deposito in corso, tale chiamata restituirà un token UUID.

Per confermare un deposito, l'applicazione dovrà effettuare una chiamata HTTP GET a \texttt{/deposits/store/\{token\}} in cui \texttt{\{token\}} è un parametro che indica l'UUID precedentemente ricevuto dal Service.

\subparagraph{Interazione tra Dashboard e Service}

Per intervenire sullo stato del Dumpster e portarlo in "available" o "not available" è possibile effettuare le rispettive chiamate HTTP GET: \texttt{/dumpsters/enable} e \texttt{/dumpsters/disable}.

La dashboard permette di visualizzare l'andamento di utilizzo del Dumpster negli ultimi \texttt{N} giorni attraverso la chiamata HTTP GET \texttt{/dumpsters/depositsAmount} a cui è necessario passare il numero di giorni di cui si vogliono ottenere informazioni.

È inoltre possibile, attraverso la Dashboard, modificare l'indirizzo Internet mediante il quale è possibile raggiungere il modulo "Edge" al fine di interagire con esso. Per fare ciò il Service mette a disposizione la chiamata HTTP PATCH \texttt{/dumpsters/setInternetAddress}.

\newpage

\paragraph{Modello dei dati}

Si è ritenuto opportuno affiancare una base dati al backend, al fine di agevolare la gestione delle richieste fatte dagli utenti dell'applicazione (in particolare l'assegnazione del token) e memorizzare il peso del dispositivo ottenuto dal componente Edge.

La base dati ideata, pur rimanendo molto semplice come quantità di attributi immagazzinabili, risponde ai requisiti di progetto.

\begin{figure}[!htb]
	\centering
	\includegraphics[scale=0.5]{./img/SmartDumpster.png}
	\caption{Schema logico relazionale della base dati.}\label{fig:relational}
\end{figure}

Come osservabile in figura \ref{fig:relational}, il modello dei dati comprende le entità \textit{Dumpster} e \textit{Deposito}. Da tale modellazione, e come deducibile dalle considerazioni effettuate al capitolo \ref{sec:sviluppi-futuri}, è possibile derivare la possibilità di gestire più moduli "Edge" del sistema "Smart Dumpster".

\newpage

\subsubsection{App}

Per gestire le comunicazioni App-Controller tramite il modulo bluetooth, è stata creata la classe BluetoothThread. Quest'ultima, seguendo il pattern singleton, si occupa dei vari dialoghi che avvengono, per l'appunto, tra l'App ed il Controller. Ad ogni activity viene presentata un'interfaccia molto semplificata, la quale espone solo le funzionalità essenziali. Viene data la possibilità di specificare un messaggio JSON da inviare al Controller e di iscriversi a determinati eventi (e.g. \texttt{onEchoReceived}).

\subsubsection{Protocollo di comunicazione}

L'interazione tra i diversi componenti in termini di chiamate da effettuare per completare le diverse operazioni è stata mostrata e motivata nelle sezioni precedenti.

Segue quindi una descrizione della struttura e del dominio dei messaggi scambiati (in formato JSON) tra i diversi moduli del sistema.

Si precisa che il flusso di controllo di una tipica operazione di deposito di un rifiuto è mostrato nel diagramma di sequenza in figura \ref{fig:sequence}.

\paragraph{Comunicazione tra App e Controller}
\label{par:app-controller-communication}

Di seguito vengono mostrati i messaggi scambiati tra App e Controller relativi all'operazione di deposito di un rifiuto.
Si noti che all'invio di ogni messaggio, l'interlocutore risponde con una copia dello stesso al fine di confermarne la ricezione, come descritto nel paragrafo \ref{sec:controllerdesign}.

\subparagraph{Messaggi generati dall'App}

Per richiedere l'apertura dello sportello del Dumpster, una volta ottenuto un token, l'Applicazione invia al Controller via Bluetooth un messaggio JSON indicando inoltre il tipo di rifiuto che si vuole depositare, come nell'esempio che segue:

\begin{minted}{json}
{
    "request": "open",
    "trashtype": 0
}
\end{minted}

Una volta aperto lo sportello, in caso sia necessario più tempo per il deposito di un rifiuto, l'utente può, tramite l'Applicazione, effettuare la richiesta che verrà rappresentata dal seguente messaggio:

\begin{minted}{json}
{
    "request": "extend_time",
    "amount": 30
}
\end{minted}

\subparagraph{Messaggi generati dal Controller}

Il Controller, in seguito alle richieste dell'Applicazione, informa quest'ultima sullo stato in cui si trova lo sportello. Il campo \texttt{event} può quindi assumere i seguenti valori: \texttt{closed}, \texttt{closing}, \texttt{opened}, \texttt{opening}.

\begin{minted}{json}
{
    "event": "closed"
}
\end{minted}

\paragraph{Comunicazione tra App e Service}

In generale, ad ogni richiesta, il modulo Service risponde con un messaggio JSON costituito da un codice e dal messaggio ad esso associato, in tale modo qualsiasi risorsa "client" che accede al modulo Service, può facilmente distinguere nelle risposte gli errori da altri tipi di messaggi.

In seguito alla richiesta di un token il Service invierà un messaggio come quello in esempio:

\begin{minted}{json}
{
    "code": 200,
    "message": "Dumpster available for you!",
    "token": <UUID>
}
\end{minted}

Il comportamento è analogo per tutte le altre operazioni supportate dal modulo Service. Per ulteriori informazioni e per osservare i codici e i messaggi di errore si consulti il codice sorgente.

\paragraph{Comunicazione tra Service e Edge}

L'interazione tra i moduli Service e Edge segue quanto scritto nella sezione \ref{sec:edge-detailed} circa l'utilizzo dell'implementazione fornita da Mozilla per l'architettura Web of Things.

In seguito alla richiesta di spegnimento di un LED, il modulo Edge manderà una risposta in formato JSON come nell'esempio che segue:

\begin{minted}{json}
{
    "on": false
}
\end{minted}

\newpage

\subsection{Diagramma di sequenza}

In figura \ref{fig:sequence} è mostrato il diagramma di sequenza per l'operazione di deposito di un rifiuto nel Dumpster.

L'operazione di controllo del peso interrogando il modulo Edge è stata modularizzata per poter essere riutilizzata in più punti dello schema ed è mostrata in figura \ref{fig:sequence-get-weight}.

\begin{figure}[!htbp]
	\centering
	\begin{plantuml}
		@startuml
participant Service
participant Edge

Service->Edge : Request dumpster weight
Service<--Edge : Send dumpster weight

opt Weight over limit
Service->Service : Set dumpster unavailable
end
		@enduml
	\end{plantuml}
	\caption{Diagramma di sequenza per l'operazione di controllo del peso del Dumpster.}
	\label{fig:sequence-get-weight}
\end{figure}

\begin{landscape}

\begin{figure}[!htbp]
	\centering
	\vspace*{-3cm}
	\begin{plantuml}
		@startuml
actor User
participant MobileApp
participant Service
participant Edge
participant Controller

User->MobileApp : Request dumpster use
MobileApp->Service : Request token

ref over Edge, Service : Get dumpster weight

alt Dumpster available

MobileApp<--Service : Send token
User->MobileApp : Select trash type
MobileApp->>Controller : Send dumpster activate request
Controller->Controller : Turn on trash type LED
Controller->Controller : Open dumpster
Controller->Controller : Wait for Tdeliver seconds
MobileApp->>Controller : Extend time
Controller -> Controller : Extend time

Controller->Controller : Close dumpster
MobileApp<--Controller : Report successful dumpster activation
User<--MobileApp : Show successful dumpster activation
MobileApp->>Service : Report successful dumpster activation

ref over Edge, Service : Get dumpster weight
Service->Service : Save dumpster weight

opt Weight changed
Service->Service: Increase trash deposit count
end

else Dumpster unavailable

MobileApp<--Service : Send error
User<--MobileApp : Show error

end
		@enduml
	\end{plantuml}
	\caption{Diagramma di sequenza per l'operazione di deposito di rifiuti nel sistema Smart Dumpster.}
	\label{fig:sequence}
\end{figure}
\end{landscape}

\newpage

\subsection{Schema del circuito}

La fase di progettazione del sistema comprende anche la pianificazione della disposizione e del collegamento dei componenti fisici prima dell'effettiva realizzazione su breadboard.

\begin{figure}[!ht]
	\includegraphics[scale=0.4]{./fritzing/controller.png}
	\caption{Schema in Fritzing del circuito relativo al modulo Controller.}\label{fig:fritzing}
\end{figure}

\begin{figure}[!ht]
	\includegraphics[scale=0.4]{./fritzing/edge.png}
	\caption{Schema in Fritzing del circuito relativo al modulo Edge.}\label{fig:fritzing_edge}
\end{figure}
