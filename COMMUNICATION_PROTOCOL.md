# Communication protocol

## App <-> Controller

### Data structure and domain

**Controller -> App**:

- `event` : `closed` / `closing` / `opened`

example

```json
{
  "event": "closed"
}
```

**App -> Controller**:

- `request` :
  - `open`
    - `trashtype` : integer (number of the ENUM)
  - `extend_time`
    - `amount`: integer

example

```json
{
  "request": "extend_time",
  "amount": 30
}
```

### Data flow


* App -> Controller
```json
{
  "request": "open"
  "trashtype": 0 (number of the ENUM)
}
```
        
* Controller -> App
```json
{
  "event": "opened"
}
```

* App -> Controller
```json
{
  "request": "extend_time",
  "amount": 30
}
```

* Controller -> App
```json
{
  "event": "closing"
}
```

* Controller -> App
```json
{
  "event": "closed"
}
```

## Service <-> Edge communication

### Data structure and domain

Puntare / spiegare documentazione Mozilla WoT

### Data flow

- richiesta valore peso (lettura potenziometro)
- lettura valore led
- scrittura valore led (stato)

## Service <-> App communication

### API calls

- richiesta token `/dumpsters/getToken`
- conferma deposito (triggera il service per andare a leggere il peso sull'edge) `/deposits/store/{token}`

### Data structure and domain

- `code`: integer
- `message`: string
- `token`: string (from UUID; valid only for `/dumpsters/getToken` API call)

Example:

```json
{
  "code": 200,
  "message": "Dumpster [{id}] available for you"
}
```

### Data flow

