# Status

## Edge

```plantuml
@startuml
hide empty description

[*] --> Available
Available --> Unavailable : Weight\nover limit || \n Force unavailable
Available : do/ L_avail
Unavailable --> Available : Reset
Unavailable : do/ L_not_avail

Available --> Available : Request for weight / \n Measure weight

@enduml
```

## Controller

```plantuml
@startuml
hide empty description

[*] --> Closed : Set tMax
Closed  : do/Listen for requests
Closing : entry/L_x OFF
Closing : do/Close lid
Closed --> Opening : Deposit\nrequest/\nt = T_deliver
Opened --> Closing : t = 0
Closing --> Closed : Lid closed

Opening : entry/Open lid
Opening : entry/L_x ON
Opening : entry/Start timer

Opening --> Opened

Opened --> Opened : Ask for more time \n[t < tMax]\n/ t += T_amount

@enduml
```

## MobileApp

```plantuml
@startuml
hide empty description

[*] --> Init
Init --> WaitToken : Token\nrequest
WaitToken --> Init : Dumpster\nunavailable or \n reserved
WaitToken --> ChooseGarbageType : Dumpster\navailable
ChooseGarbageType --> Deposit : Send garbage type \n/ t = T_deliver
Deposit --> RequestedTimeExtension : Ask \nfor more time
RequestedTimeExtension --> Deposit : t += T_amount
Deposit --> Timeout : t = 0
Timeout : do/Wait for confirmation
Timeout --> Init : Confirm deposit

@enduml
```
