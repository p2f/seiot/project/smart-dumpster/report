# Edge diagram

```plantuml
@startuml
class Edge {}

interface AsyncWiFiManager << external >> {
    + bool autoConnect(char*, char*)
}

interface WebThingAdapter << external >> {
    + void begin()
}
interface ThingDevice {
    + String description
}
interface ThingProperty {
    + String title
    + void setValue(ThingPropertyValue)
    + ThingPropertyValue changedValueOrNull()
    + ThingPropertyValue getValue()
}
interface ThingPropertyValue {}

interface Led {
    + void switchOn()
    + void switchOff()
    + bool isTurnedOn()
}

class LedImpl {
    - uint pin
    - bool isOn
    + LedImpl(uint pin)
    + void switchOn()
    + void switchOff()
    + bool isTurnedOn()
}

Led <|-- LedImpl
Edge "1" *-right- "1" Potentiometer
Edge "1" *-- "2" Led
Edge "1" *-left- "1" AsyncWiFiManager
Edge "1" *-- "1" WebThingAdapter
WebThingAdapter "1" o-- "3" ThingDevice
ThingDevice o-- ThingProperty
ThingProperty o-- ThingPropertyValue

Potentiometer <|-- PotentiometerImpl

interface Potentiometer {
    + uint read()
}

class PotentiometerImpl {
    - uint pin
    - uint min
    - uint max
    + uint PotentiometerImpl(uint pin, uint min, uint max)
    + uint read()
}

@enduml
```

# Controller diagram

```plantuml
skinparam groupInheritance 3
top to bottom direction

interface Task {
    + void init()
    + void tick()
}

interface Scheduler {
    - Task *tasks
    + void init()
    + void schedule()
    + bool addTask()
}

Scheduler "1" *-- "*" Task

class OpeningTask
class OpenedTask
class ClosingTask
class ClosedTask

Task <|--- OpeningTask
Task <|--- OpenedTask
Task <|--- ClosingTask
Task <|--- ClosedTask
```

```plantuml
class OpeningTask
class ClosingTask

enum TrashType {
    PAPER
    PLASTIC
    GLASS
}

enum StateType {
    CLOSED
    OPENING
    OPENED
    CLOSING
}

interface ServoMotor {
    + uint getPosition()
    + void setPosition(uint angle)
    + void moveLeft()
    + void moveRight()
}

class ServoMotorImpl {
    - uint pin
    - ServoTimer2 motor
    + ServoImpl(uint pin)
    + uint getPosition()
    + void setPosition(uint angle)
    + void moveLeft()
    + void moveRight()
}

class ServoTimer2 <<external>> {
    + attach();
    + detach();
    + write(int);
}

interface Led {
    + void switchOn()
    + void switchOff()
    + bool isTurnedOn()
}

class LedImpl {
    - uint pin
    - bool isOn
    + LedImpl(uint pin)
    + void switchOn()
    + void switchOff()
    + bool isTurnedOn()
}

interface JsonMsgService {
    + String getEvent()
    + TrashType getTrashType()
    + String getRequest()
    + uint getAmount()
    + bool isJsonAvailable()
    + void sendEvent(String)
    + void sendExtend(uint)
    + void sendEcho()
}

interface BluetoothMsgService {
    + void init()
    + bool isMsgAvailable()
    + Msg* receiveMsg()
    + void sendMsg(String)
}

TrashType <.. OpeningTask: <<use>>
TrashType <.. ClosingTask: <<use>>
OpeningTask "1" o-- "1" ServoMotor
ClosingTask "1" o-- "1" ServoMotor
OpeningTask "3" o-- "1" Led
ClosingTask "3" o-- "1" Led
ServoMotor <|-- ServoMotorImpl
Led <|-down- LedImpl
ServoMotorImpl "1" o-down- "1" ServoTimer2 : <<external>>

OpeningTask ..> JsonMsgService : <<use>>
OpenedTask ..> JsonMsgService : <<use>>
ClosingTask ..> JsonMsgService : <<use>>
ClosedTask ..> JsonMsgService : <<use>>

OpeningTask .up.> StateType : <<use>>
OpenedTask .up.> StateType : <<use>>
ClosingTask .up.> StateType : <<use>>
ClosedTask .up.> StateType : <<use>>

class BluetoothMsgService

JsonMsgService *-- BluetoothMsgService
```


